#!/bin/bash

docker stop benchmarkserver
docker rm benchmarkserver
docker build --force-rm -f Dockerfile -t localbuild .
echo "build benchmark"
docker build --force-rm -f ./benchmark/BenchmarkDockerfile -t benchmarkbuild .

docker run -d --name benchmarkserver -p 1234:80 benchmarkbuild 

echo "RUN AB TEST"
echo "-- might be run from another machine"
ab -n 1000 -c 20 http://localhost:1234/