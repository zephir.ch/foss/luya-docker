FROM alpine:3.18

RUN apk update && apk add --no-cache \
    supervisor \
    curl \
    fcgi \
    busybox \
    nginx \
    vips \
    imagemagick \
    php82 \
    php82-fpm \
    php82-cli \
    php82-exif \
    php82-ctype \
    php82-mongodb \
    php82-pdo \
    php82-pdo_mysql \
    php82-json \ 
    php82-curl \ 
    php82-xml \
    php82-xmlwriter \
    php82-intl \
    php82-dom \
    php82-mbstring \
    php82-gd \
    php82-zip \
    php82-session \
    php82-opcache \
    php82-tokenizer \
    php82-fileinfo \
    php82-simplexml \
    php82-iconv \
    php82-pecl-memcached \
    php82-pecl-igbinary \
    php82-sockets \
    php82-pcntl \
    php82-pecl-imagick \
    php82-ffi \
    icu-data-full

# Healtcheck Script
# see: https://github.com/renatomefi/php-fpm-healthcheck
RUN wget -O /usr/local/bin/php-fpm-healthcheck \
    https://raw.githubusercontent.com/renatomefi/php-fpm-healthcheck/master/php-fpm-healthcheck \
    && chmod +x /usr/local/bin/php-fpm-healthcheck

RUN ln -sf /usr/bin/php82 /usr/bin/php

COPY docker /
COPY env.php /var/www/html/configs/env.php

RUN mkdir -p /run/nginx

WORKDIR /var/www/html

EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]

HEALTHCHECK --interval=15s CMD php-fpm-healthcheck || exit 1
