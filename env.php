<?php		
         
$config = require 'config.php'; 

$env = getenv('LUYA_CONFIG_ENV');

if (empty($env)) {
    $env = \luya\Config::ENV_PROD;
}

$appVersion = getenv('LUYA_APP_VERSION');

if (!empty($appVersion)) {
    $config->application([
        'version' => $appVersion,
    ]);
}

return $config->toArray([$env]);
