# PRODUCTION READY LUYA DOCKER IMAGE

This is a production ready LUYA Website Docker image based on **PHP 8.2 FPM and NGINX**. The default php.ini configuration enables strong caching settings trough opcache and is therefore not recommend for developement purposes.

## Dockerfile

Using as a  `Dockerfile`:

```docker
FROM registry.gitlab.com/zephir.ch/foss/luya-docker:php82

## Replace the default server name `luya` with your own server name
RUN sed -i 's/server_name luya;/server_name MY_SUPER_WEBSITE.COM;/g' /etc/nginx/conf.d/default.conf

COPY . /var/www/html

RUN mkdir -p /var/www/html/public_html/assets
RUN mkdir -p /var/www/html/runtime

RUN chmod 777 /var/www/html/public_html/assets
RUN chmod 777 /var/www/html/runtime
```

## ENV Variables

The dockerfile supports the following env variables which can be set during runtime:

|Name|Example|Description
|----|-------|-----------
|`LUYA_APP_VERSION`|`1.2.0`|The version of the Application. Can be used later with Yii::$app->version. Default is `1.0`.
|`LUYA_CONFIG_ENV`|`prod`|The environment which should be taken to the load the config, Default is `prod`.

## App Version wihle building

In order to set the application version while building the docker image you can provided the version (which might be the release tag) as build arg and then assign the arg value into the env variable:

```
ARG VERSION=dev
ENV LUYA_APP_VERSION=$VERSION
```

In your pipeline the build step might look like this:

```
docker build -t ... --build-arg VERSION=$CI_COMMIT_TAG .
```

## Docker Compose (local developement)

Using the image inside a docker-compose.yml file is **not recommend due to heavy PHP Opcache settings**. In order to use in docker-compose context we recommend to have a `localphp.ini` which overrides production opcache caching:

```
[opcache]
opcache.validate_timestamps=1
opcache.revalidate_freq=1
[PHP]
display_errors = On
log_errors = On
error_reporting = E_ALL
```

Then bind the ini in docker-compose.yml:

```
luya_web:
    image: registry.gitlab.com/zephir.ch/foss/luya-docker:php82
    volumes:
      - ./localphp.ini:/etc/php82/conf.d/zzz_custom.ini
      - ./:/var/www/html
   healthcheck:
      disable: true
```

## Customize Config

Copy the custom nginx config into the Dockerfile:

```
COPY nginx.conf /etc/nginx/conf.d/default.conf
```

Copy a custom php ini into the Dockerfile:

```
COPY php.ini /etc/php82/conf.d/custom.ini
```

If you just want to change certain values, do not override the custom.ini from the Dockerfile, append your custom file instead:

```
COPY php.ini /etc/php82/conf.d/zzz_custom.ini
```

## About Size, Memory and FPM Pool

The image has by default a max php memory limit of `128MB` and we calculate an avarage of `5MB` for each request. `128MB / 5MB` are `25` child processes which can be allocated (`pm.max_children = 20`)

In order to test the avarage fpm load add `apk add --no-cache procps` and run `ps -eo size,pid,user,command --sort -size | awk '{ hr=$1/1024 ; printf("%13.2f Mb ",hr) } { for ( x=4 ; x<=NF ; x++ ) { printf("%s ",$x) } print "" }' | grep php-fpm`.

## Healthcheck

The fpm process has setup for ping `curl localhost/ping` and status `curl localhost/status`. Those pages are only callable from localhost. For further healthcheck with in combination of Kubernetes we have integrated the [FPM Healtcheck Script](https://github.com/renatomefi/php-fpm-healthcheck) you can call this inside the container with `php-fpm-healthcheck -v`. Here is an example of how to integrate with k8s: https://github.com/renatomefi/php-fpm-healthcheck#kubernetes-example

```yaml
livenessProbe:
   exec:
      command:
         - timeout
         - "1"
         - php-fpm-healthcheck
   initialDelaySeconds: 5
   periodSeconds: 10
readinessProbe:
   exec:
      command:
         - php-fpm-healthcheck
   initialDelaySeconds: 5
   periodSeconds: 10
```

> The liveness probe requires a timeout, see: https://github.com/renatomefi/php-fpm-healthcheck/issues/41

## Build Testing

```
docker build --no-cache -t phpnginx82 . && docker run -p 1234:80 phpnginx82
```

## Benchmark

The test is running on the same machine where docker command (from above) is running.

```
ab -n 1000 -c 20 http://localhost:1234/
```

```
Concurrency Level:      20
Time taken for tests:   3.068 seconds
Complete requests:      1000
Failed requests:        0
Total transferred:      259000 bytes
HTML transferred:       15000 bytes
Requests per second:    325.90 [#/sec] (mean)
Time per request:       61.369 [ms] (mean)
Time per request:       3.068 [ms] (mean, across all concurrent requests)
Transfer rate:          82.43 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.3      0       2
Processing:     1   61 384.8      2    2897
Waiting:        1   61 384.8      2    2897
Total:          1   61 384.9      3    2898
```

> Failed requests are only length requests, might be cause the phpinfo changes content length: https://serverfault.com/a/414808

## References & Resources

+ https://www.nginx.com/blog/avoiding-top-10-nginx-configuration-mistakes
+ https://serverfault.com/a/922983
+ https://www.nginx.com/blog/tuning-nginx/
+ https://www.digitalocean.com/community/tutorials/how-to-optimize-nginx-configuration
